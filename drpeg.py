from utils import constants


constants(globals(), (
    "UP",
    "DOWN_RIGHT",
    "BOTTOM",
))


class Node(object):
    # (keep match, direction, y, ...)
    y = None
    n = None
    n0 = None

    up = None
    right = None
    down = None

    start = None

    # bottom-up
    def __init__(self):
        raise Exception("Not implemented")

    # top-down
    def prep(self):
        raise Exception("Not implemented")


class T(Node):
    y = (True, UP, True)
    n0 = (False, UP, False)

    name = None

    def __init__(self, text):
        self.text = text

    def prep(self, rules):
        if self.name is not None:
            self.named = self
        elif self.up:
            self.named = self.up
        return ()


class Y(Node):
    y = (True, UP, True)
    n0 = (False, UP, False)

    def __init__(self, down_name, *, name=None):
        self.down_name = down_name
        self.name = name

    def prep(self, rules):
        self.down_base = rules[self.down_name]

    @property
    def down(self):
        x = self.down_base.clone(self)
        return x


class UnaryNode(Node):
    def __init__(self, down, *, name=None):
        self.down = down
        self.name = name

    def prep(self):
        self.named = self.down.named
        if self.name is not None:
            self.named.append(self)


class L(UnaryNode):
    y = (False, UP, True)
    n0 = (False, UP, False)


class N(UnaryNode):
    y = (False, UP, False)
    n0 = (False, UP, True)

    def __init__(self, down, *, name=None):
        self.down = down
        self.name = name


class Q(UnaryNode):
    y = (True, UP, True)
    n0 = (False, UP, True)


class S(UnaryNode):
    y = (True, BOTTOM, None)
    n = (True, UP, True)
    n0 = (True, UP, True)


class P(UnaryNode):
    y = (True, BOTTOM, None)
    n = (True, UP, True)
    n0 = (False, UP, False)


class ListNode(Node):
    def __init__(self, children, *, name=None):
        self.children = tuple(children)
        if len(self.children) == 0:
            raise Exception("Need at least one child")
        self.down = self.children[0]
        prev = None
        for c in reversed(children):
            c.up = self
            c.right = prev
            prev = c
        self.name = name

    def prep(self):
        self.named = self.down.named
        if self.name is not None:
            self.named.append(self)


class G(ListNode):
    y = (True, DOWN_RIGHT, None, UP, True)
    n0 = (False, UP, False)


class A(ListNode):
    y = (True, UP, True)
    n0 = (False, DOWN_RIGHT, None)
